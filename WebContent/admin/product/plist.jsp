<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1 align="left" style="color:blue">所有商品</h1>
	<form action="${pageContext.request.contextPath}/" method="post">
	<div>
		<!--显示商品信息  -->
		<table cellspacing="0" cellpadding="1" rules="all"
								bordercolor="gray" border="1" id="DataGrid1"
								style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; WIDTH: 100%; WORD-BREAK: break-all; BORDER-BOTTOM: gray 1px solid; BORDER-COLLAPSE: collapse; BACKGROUND-COLOR: #f5fafe; WORD-WRAP: break-word">
			<tr style="FONT-WEIGHT: bold; FONT-SIZE: 12pt; HEIGHT: 25px; BACKGROUND-COLOR: #afd1f3">
				<th>序号</th>
				<th style="background-color: silver;">商品名称</th>
				<th style="background-color: silver;">市场价</th>
				<th style="background-color: silver;">商城价</th>
				<th style="background-color: silver;">商品图片</th>
				<th style="background-color: silver;">商品日期</th>
				<th style="background-color: silver;">是否热门</th>
				<th style="background-color: silver;">商品分类</th>
				<th style="background-color: silver;">商品描述</th>
				<th style="background-color: silver;">操作</th>
			</tr>
				<c:if test="${not empty pb && not empty pb.result }">
					<c:forEach items="${pb.result }" var="product" varStatus="vs">
						<tr align="center" onmouseover="this.style.backgroundColor = 'white'"
									onmouseout="this.style.backgroundColor = '#F5FAFE';">
							<td>${vs.count }</td>
							<td >${product.pname }</td>
							<td >${product.market_price }</td>
							<td >${product.shop_price }</td>
							<td >
								<img src="${product.pimage }" alt="图片加载失败" width="50px" height="50px"/>
							</td>
							<td >${product.pdate }</td>
							<td >
								<c:choose>
									<c:when test="${product.is_hot == 1}">
										热门
									</c:when>
									<c:otherwise>
										不热门
									</c:otherwise>
								</c:choose>
							</td>
							<td >${product.category.cname }</td>
							<td width="300px">
								<textarea rows="4" cols="45" disabled="disabled">${product.pdesc }</textarea>
							</td>
							<td >
								<a href="${pageContext.request.contextPath}/ProductServlet?method="><input type="button" value="修改"></a>
								<a href="${pageContext.request.contextPath}/ProductServlet?method=deleteProduct&pid=${product.pid}" onclick="confirmDel()"><input type="button" value="删除"></a>
							</td>
						</tr>
					</c:forEach>
				</c:if>
		
	
		<!--分页 -->
			<tr colspan="9" align="right">
				<!-- <ul class="pagination"> -->
					<!-- 上一页 -->
					<c:if test="${not empty pb}">
					<c:choose>
						<c:when test="${pb.pageNumber == 1}">
							<!-- <li class="disabled"> -->
								<a href="javaScript:void(0)" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							<!-- </li> -->
						</c:when>
						<c:otherwise>
							<!-- <li class=""> -->
								<a href="${pageContext.request.contextPath }/ProductServlet?method=findAllProductForPage&pageNumber=${pb.pageNumber-1 }" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							<!-- </li> -->
						</c:otherwise>
					</c:choose>
				<!-- 当前页 -->
					<c:forEach begin="1" end="${pb.totalPage }" var="vs">
						<c:choose>
							<c:when test="${pb.pageNumber == vs }">
								<!-- <li class="disabled"> -->
									<a href="javaScript:void(0)">${vs }</a>
								<!-- </li> -->
							</c:when>
							<c:otherwise>
								<!-- <li class=""> -->
									<a href="${pageContext.request.contextPath }/ProductServlet?method=findAllProductForPage&pageNumber=${vs}">${vs }</a>
								<!-- </li> -->
							</c:otherwise>
						</c:choose>
					</c:forEach>
				<!-- 下一页 -->
					<c:choose>
						<c:when test="${pb.pageNumber == pb.totalPage}">
							<!-- <li class="disabled"> -->
								<a href="javaScript:void(0)" aria-label="">
									<span aria-hidden="true">&raquo;</span>
								</a>
							<!-- </li> -->
						</c:when>
						<c:otherwise>
							<!-- <li class=""> -->
								<a href="${pageContext.request.contextPath }/ProductServlet?method=findAllProductForPage&pageNumber=${pb.pageNumber+1 }" aria-label="Previous">
									<span aria-hidden="true">&raquo;</span>
								</a>
							<!-- </li> -->
						</c:otherwise>
					</c:choose>
				</c:if>
				<!-- </ul> -->
			</tr>
		<!-- 分页结束=======================-->
		</table>
	</div>
	</form>
</body>
</html>