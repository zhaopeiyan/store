<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<HTML>
	<HEAD>
		<meta http-equiv="Content-Language" content="zh-cn">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="${pageContext.request.contextPath}/css/Style1.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
		<script language="javascript" src="${pageContext.request.contextPath}/layer/layer.js"></script>
		<script type="text/javascript">
			
			function orderDetial(oid) {
				
				$.post("${pageContext.request.contextPath}/manageServlet?method=findOrderByOid",{oid:oid},function (data){
					tbl = '<table border="1" width="100%" align="center"><tr><th>商品名称</th><th>购买数量</th><th>小计</th></tr>';
					$(data.list).each(function(){
						alert(this.product.pname);
						alert(123);
						tbl+="<tr><td>"+this.product.pname+"</td><td>"+this.count+"</td><td>"+this.subtotal+"</td></tr>";
					});
					tbl+="</table>";						
				},"json")
				
				layer.open({
					type:1,
					title:['订单详情'],
					area:['650px','250px'],
					shade:0.3,
					closeBtn:1,
					maxmin:true,
					shadeClose:true,
					content:tbl
				});
			}
		</script>
	</HEAD>
	<body>
		<br>
		<form id="Form1" name="Form1" action="${pageContext.request.contextPath}/user/list.jsp" method="post">
			<table cellSpacing="1" cellPadding="0" width="100%" align="center" bgColor="#f5fafe" border="0">
				<TBODY>
					<tr>
						<td class="ta_01" align="center" bgColor="#afd1f3">
							<strong>订单列表</strong>
						</TD>
					</tr>
					
					<tr>
						<td class="ta_01" align="center" bgColor="#f5fafe">
							<table cellspacing="0" cellpadding="1" rules="all"
								bordercolor="gray" border="1" id="DataGrid1"
								style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; WIDTH: 100%; WORD-BREAK: break-all; BORDER-BOTTOM: gray 1px solid; BORDER-COLLAPSE: collapse; BACKGROUND-COLOR: #f5fafe; WORD-WRAP: break-word">
								<tr
									style="FONT-WEIGHT: bold; FONT-SIZE: 12pt; HEIGHT: 25px; BACKGROUND-COLOR: #afd1f3">

									<td align="center" width="10%">
										序号
									</td>
									<td align="center" width="10%">
										订单编号
									</td>
									<td align="center" width="10%">
										订单金额
									</td>
									<td align="center" width="10%">
										收货人
									</td>
									<td align="center" width="10%">
										订单状态
									</td>
									<td align="center" width="50%">
										订单详情
									</td>
								</tr>
							<c:if test="${not empty pb && not empty pb.result}">
								<c:forEach items="${pb.result }" var="order" varStatus="vs">
									<tr onmouseover="this.style.backgroundColor = 'white'"
									onmouseout="this.style.backgroundColor = '#F5FAFE';">
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="18%">
										${vs.count }
									</td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="17%">
										${order.oid }
									</td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="17%">
										${order.total }
									</td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="17%">
										${order.name }
									</td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="17%">
										<c:choose>
											<c:when test="${order.state == 0 }">
												未支付
											</c:when>
											<c:when test="${order.state == 1 }">
												已支付，未发货
											</c:when>
											<c:when test="${order.state == 2 }">
												已发货，未签收
											</c:when>
										</c:choose>
									</td>
									<td align="center" style="HEIGHT: 22px">
										<a href="javascript:void(0);" onclick="orderDetial('${order.oid}')">
											<input type="button" value="订单详情" />
										</a>
										<a href="javascript:void(0);" onclick="orderDetial'${order.oid}')">订单详情</a>
									</td>
								</tr>
								</c:forEach>
							</c:if>
							
							
							
							<!--分页 -->
			<tr colspan="9" align="right">
				<!-- <ul class="pagination"> -->
					<!-- 上一页 -->
					<c:if test="${not empty pb}">
					<c:choose>
						<c:when test="${pb.pageNumber == 1}">
							<!-- <li class="disabled"> -->
								<a href="javaScript:void(0)" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							<!-- </li> -->
						</c:when>
						<c:otherwise>
							<!-- <li class=""> -->
								<a href="${pageContext.request.contextPath }/manageServlet?method=findAllOrderForPage&pageNumber=${pb.pageNumber-1 }" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							<!-- </li> -->
						</c:otherwise>
					</c:choose>
				<!-- 当前页 -->
					<c:forEach begin="1" end="${pb.totalPage }" var="vs">
						<c:choose>
							<c:when test="${pb.pageNumber == vs }">
								<!-- <li class="disabled"> -->
									<a href="javaScript:void(0)">${vs }</a>
								<!-- </li> -->
							</c:when>
							<c:otherwise>
								<!-- <li class=""> -->
									<a href="${pageContext.request.contextPath }/manageServlet?method=findAllOrderForPage&pageNumber=${vs}">${vs }</a>
								<!-- </li> -->
							</c:otherwise>
						</c:choose>
					</c:forEach>
				<!-- 下一页 -->
					<c:choose>
						<c:when test="${pb.pageNumber == pb.totalPage}">
							<!-- <li class="disabled"> -->
								<a href="javaScript:void(0)" aria-label="">
									<span aria-hidden="true">&raquo;</span>
								</a>
							<!-- </li> -->
						</c:when>
						<c:otherwise>
							<!-- <li class=""> -->
								<a href="${pageContext.request.contextPath }/manageServlet?method=findAllOrderForPage&pageNumber=${pb.pageNumber+1 }" aria-label="Previous">
									<span aria-hidden="true">&raquo;</span>
								</a>
							<!-- </li> -->
						</c:otherwise>
					</c:choose>
				</c:if>
				<!-- </ul> -->
			</tr>
		<!-- 分页结束=======================-->
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
		</form>
	</body>
</HTML>

