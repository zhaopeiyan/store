<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/jquerytree/jquery.treeview.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/jquerytree/screen.css" />
<script src="${pageContext.request.contextPath}/js/jquerytree/jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquerytree/jquery.cookie.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquerytree/jquery.treeview.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script type="text/javascript">
$(document).ready(function(){
	$("#browser").treeview();
});
</script>
<body>
	<ul id="browser" class="filetree">
		<li><span class="folder">分类管理</span>
			<ul>
				<li><span class="file"><a href="${pageContext.request.contextPath}/ProductServlet?method=findAllCategory" target="right">查看分类</a></span></li>
				<li><span class="file"><a href="${pageContext.request.contextPath}/admin/category/save.jsp" target="right">添加分类</a></span></li>
			</ul>
		</li>
		<li><span class="folder">商品管理</span>
			<ul>
				<li><span class="file"><a href="${pageContext.request.contextPath}/ProductServlet?method=findAllProductForPage&pageNumber=1" target="right">查看商品信息</a></span></li>
				<li><span class="file"><a href="${pageContext.request.contextPath}/ProductServlet?method=findAllCategoryForProduct&come=add" target="right">添加商品</a></span></li>
			</ul>
		</li>
		<li><span class="folder">订单管理</span>
			<ul>
				<li><span class="file"><a href="${pageContext.request.contextPath}/manageServlet?method=findAllOrderForPage&pageNumber=1" target="right">查看订单</a></span></li>
			</ul>
		</li>
	</ul>
</body>
</html>