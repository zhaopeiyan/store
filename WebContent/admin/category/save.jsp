<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1 align="center">分类信息添加</h1>
	<form action="${pageContext.request.contextPath}/ProductServlet?method=addCategory" method="post" class="form">
		<table align="center" width="80%" cellspacing="1" cellpadding="1">
			<tr>
				<th style="background-color: silver;">分类名称</th>
				<td colspan="2">
					<input type="text" name="cname" size="75" />
				</td>
			</tr>
			<tr>
				<th style="background-color: silver;"></th>
				<td colspan="2">
					<input type="submit" value="添加"/>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>