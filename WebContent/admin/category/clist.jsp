<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1 align="center">分类信息查看</h1>
	<table align="center" width="80%" cellspacing="1" cellpadding="1" border="1px">
		<tr align="center" style="background-color: silver;">
			<th>序号</th>
			<th>分类名称</th>
			<th>操作</th>
		</tr>
		<c:if test="${not empty categorys }">
			<c:forEach items="${categorys }" var="category" varStatus="vs">
			<tr align="center">
				<td>${vs.count }</td>
				<td>${category.cname }</td>
				<td>
<%-- 					<a href="${pageContext.request.contextPath}/ProductServlet?method=modifyCategoryByCid&cid=${category.cid}"> --%>
					<a href="${pageContext.request.contextPath}/ProductServlet?method=findCategoryByCid&cid=${category.cid}">
						<input type="button" value="修改" >
					</a>
					<a href="${pageContext.request.contextPath}/ProductServlet?method=delCategoryByCid&cid=${category.cid}">
						<input type="button" value="删除" >
					</a>
				</td>
			</tr>
			</c:forEach>
		</c:if>
	</table>
</body>
</html>