package cn.itcast.store.user.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.store.user.domain.User;
import cn.itcast.store.user.utils.CookieUtil;
import cn.itcast.store.user.web.servlet.IUserService;
import cn.itcast.store.user.web.servlet.impl.UserServiceImpl;

/**
 * Servlet Filter implementation class autoLoginFilter
 */
public class autoLoginFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public autoLoginFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		User user = new User();
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		Cookie[] cookies = req.getCookies();
		// 先判断session中是否有数据
		if (req.getSession().getAttribute("existUser") != null) {
//			System.out.println("服务器有session不用找cookie了！！！");
			chain.doFilter(req, res);
			return;
		} else {
			Cookie cookie = CookieUtil.findCookie(cookies, "autoLogin");
			if (cookie != null) {
//				System.out.println("找到cookie啦！！！");
				String value = cookie.getValue();
				if (value != null && value != "") {
					String[] split = value.split(":");
					user.setUsername(split[0]);
//					System.out.println(split[0]);
//					System.out.println(split[1]);
					user.setPassword(split[1]);
					IUserService us = new UserServiceImpl();
					User existUser = us.login(user);
					if (existUser != null) {
						System.out.println("cookie登陆成功了！！！");
						req.getSession().setAttribute("existUser", existUser);
					}
				}
			}
		}
		//记住用户名
		Cookie rememberUsername = CookieUtil.findCookie(cookies, "rememberUsername");
		if(rememberUsername != null){
			String value = rememberUsername.getValue();
			request.setAttribute("rememberUsername", value);
		}
		// pass the request along the filter chain
		chain.doFilter(req, res);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
