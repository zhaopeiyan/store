package cn.itcast.store.user.utils;

import javax.servlet.http.Cookie;

public class CookieUtil {

	public static Cookie findCookie(Cookie[] cookies, String name) {
		// TODO Auto-generated method stub
		Cookie cookie = null;
		if(cookies != null){
			for (Cookie c : cookies) {
				if(c.getName().equals(name)){
					cookie = c;
				}
			}
		}
		return cookie;
	}
}
