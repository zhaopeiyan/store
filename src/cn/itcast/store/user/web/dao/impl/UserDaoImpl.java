package cn.itcast.store.user.web.dao.impl;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import cn.itcast.store.user.domain.User;
import cn.itcast.store.user.web.dao.IUserDao;
import cn.itcast.store.utils.C3P0Utils;

public class UserDaoImpl implements IUserDao {

	/**
	 * 用户注册实现代码
	 */
	@Override
	public void register(User user) {
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "insert into user values (?,?,?,?,?,?,?,?,?,?)";
		Object[] params = {user.getUid(),user.getUsername(),user.getPassword(),
				user.getName(),user.getEmail(),user.getTelephone(),user.getBirthday(),
				user.getSex(),user.getState(),user.getCode()};
		try {
			qr.update(sql, params);
		} catch (Exception e) {
			throw new RuntimeException("用户注册异常" + e);
		}
	}

	/**
	 * 用户激活实现代码
	 */
	@Override
	public User findUserByCode(User user) {
		User existUser = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from user where code=?";
		try {
			existUser = qr.query(sql, new BeanHandler<>(User.class),user.getCode());
		} catch (Exception e) {
			throw new RuntimeException("激活用户异常" + e);
		}
		return existUser;
	}

	/**
	 * 用户激活后修改用户状态
	 */
	@Override
	public void activeUser(User user) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "update user set state=?,code=? where uid=?";
		Object[] params = {user.getState(),"",user.getUid()};
		try {
			qr.update(sql, params);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			throw new RuntimeException("修改用户状态异常" + e);
		}
	}

	/**
	 * 用户登录实现
	 */
	@Override
	public User login(User user) {
		// TODO Auto-generated method stub
		User existUser = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from user where username=? and password=?";
		Object[] params = {user.getUsername(),user.getPassword()};
		try {
			existUser = qr.query(sql, new BeanHandler<>(User.class), params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("用户登陆异常" + e);
		}
		return existUser;
	}

	/**
	 * 判断用户名是否可用
	 */
	@Override
	public User searchUserByName(String username) {
		// TODO Auto-generated method stub
		User existUser = null;
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "select * from user where username=?";
			existUser = qr.query(sql, new BeanHandler<>(User.class), username);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("判断用户名是否可用异常" + e);
		}
		return existUser;
	}
}
