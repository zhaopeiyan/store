package cn.itcast.store.user.web.servlet;

import cn.itcast.store.user.domain.User;

public interface IUserService {
	void register(User user);

	User findUserByCode(User user);

	void activeUser(User user);

	User login(User user);

	User searchUserByName(String username);

}
