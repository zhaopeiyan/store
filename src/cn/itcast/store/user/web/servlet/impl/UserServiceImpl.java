package cn.itcast.store.user.web.servlet.impl;

import cn.itcast.store.user.domain.User;
import cn.itcast.store.user.web.dao.IUserDao;
import cn.itcast.store.user.web.dao.impl.UserDaoImpl;
import cn.itcast.store.user.web.servlet.IUserService;

public class UserServiceImpl implements IUserService {

	@Override
	public void register(User user) {
		// TODO Auto-generated method stub
		IUserDao ud = new UserDaoImpl();
		ud.register(user);
	}

	@Override
	public User findUserByCode(User user) {
		// TODO Auto-generated method stub
		IUserDao ud = new UserDaoImpl();
		return ud.findUserByCode(user);
	}

	@Override
	public void activeUser(User user) {
		// TODO Auto-generated method stub
		IUserDao ud = new UserDaoImpl();
		ud.activeUser(user);
	}

	@Override
	public User login(User user) {
		// TODO Auto-generated method stub
		IUserDao ud = new UserDaoImpl();
		return ud.login(user);
	}

	@Override
	public User searchUserByName(String username) {
		// TODO Auto-generated method stub
		IUserDao ud = new UserDaoImpl();
		return ud.searchUserByName(username);
		
	}

}
