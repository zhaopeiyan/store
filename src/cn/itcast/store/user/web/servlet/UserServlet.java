package cn.itcast.store.user.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.store.user.domain.User;
import cn.itcast.store.user.utils.CookieUtil;
import cn.itcast.store.user.utils.MailUtils;
import cn.itcast.store.user.web.servlet.impl.UserServiceImpl;
import cn.itcast.store.utils.BaseServlet;
import cn.itcast.store.utils.UUIDUtils;

public class UserServlet extends BaseServlet {

	private static final long serialVersionUID = 1L;

	
	/**
	 * 异步校验用户名是否存在
	 * @param request
	 * @param respose
	 * @throws IOException
	 */
	public void searchUserByName(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String username = request.getParameter("username");
		IUserService us = new UserServiceImpl();
		User existUser = us.searchUserByName(username);
		if (existUser != null) {
			response.getWriter().write("false");
		} else {
			response.getWriter().write("true");
		}
	}

	/**
	 * 退出登陆，将session中保存的用户信息删除
	 * 
	 * @param request
	 * @param respose
	 * @return
	 */
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute("existUser");
		Cookie[] cookies = request.getCookies();
		Cookie cookie = CookieUtil.findCookie(cookies, "autoLogin");
		if(cookie != null){
			cookie = new Cookie("autoLogin",null);
			cookie.setPath("/");
			response.addCookie(cookie);
			System.out.println("退出用户成功,重置cookie");
		}
		return "index.jsp";
	}

	/**
	 * 用户登录功能
	 * 
	 * @param request
	 * @param respose
	 * @return
	 */
	public String login(HttpServletRequest request, HttpServletResponse response) {
		User user = new User();
		Cookie cookie = null;
		String verifyCode = request.getParameter("verifyCode");
		//System.out.println(verifyCode);
		String vCode = (String) request.getSession().getAttribute("vCode");
		//System.out.println(vCode);
		if(verifyCode.equalsIgnoreCase(vCode)){
			request.setAttribute("vmsg", "<h6 style='color:red'>验证成功</h6>");
			try {
				BeanUtils.populate(user, request.getParameterMap());
				IUserService us = new UserServiceImpl();
				User existUser = us.login(user);
				if (existUser != null) {
					request.getSession().setAttribute("existUser", existUser);
					if (request.getParameter("autoLogin") != null) {
						cookie = new Cookie("autoLogin", existUser.getUsername() + ":" + existUser.getPassword());
						cookie.setMaxAge(60 * 60);
						cookie.setPath("/");
						response.addCookie(cookie);
					} 
					if(request.getParameter("rememberUsername") != null){
						Cookie rememberUsername = new Cookie("rememberUsername",request.getParameter("username"));
						rememberUsername.setMaxAge(60 * 60);
						rememberUsername.setPath("/");
						request.getSession().setAttribute("rememberUsername", request.getParameter("username"));
						response.addCookie(rememberUsername);
					}
				}else{
					request.setAttribute("msg", "<h5 style='color:red'>登录失败</h5>");
					return "login.jsp";
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				throw new RuntimeException("用户登陆异常" + e);
			}
		}else{
			request.setAttribute("vmsg", "<h6 style='color:red'>验证失败</h6>");
			return "login.jsp";
		}
		return "index.jsp";
	}

	/**
	 * 用户邮箱激活功能
	 * 
	 * @param request
	 * @param respose
	 * @return
	 */
	public String active(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User user = new User();
		String code = request.getParameter("code");
		user.setCode(code);
		// 查询数据库中是否有相同的code
		IUserService us = new UserServiceImpl();
		User existUser = us.findUserByCode(user);
		if (existUser != null) {
			// 需要将用户的state状态设置为1，并将code删除
			existUser.setState(1);
			existUser.setCode("");
			us.activeUser(existUser);
			request.setAttribute("msg", "<h5 style='color:green'>激活成功,请登录</h5>");
		} else {
			request.setAttribute("msg", "<h5 style='color:green'>激活失败</h5>");
		}
		return "login.jsp";
	}

	/**
	 * 用户注册
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public String register(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User user = new User();
		String verifyCode = request.getParameter("verifyCode");
		//System.out.println(verifyCode);
		String vCode = (String) request.getSession().getAttribute("vCode");
		//System.out.println(vCode);
		if(verifyCode.equalsIgnoreCase(vCode)){
				request.setAttribute("vmsg", "<h6 style='color:red'>验证成功</h6>");
			try {
				BeanUtils.populate(user, request.getParameterMap());
				// 需要单独封装的数据
				user.setUid(UUIDUtils.getUUID());
				user.setCode(UUIDUtils.getUUID64());
				user.setState(0);
				// 调用service层方法，处理业务逻辑
				IUserService us = new UserServiceImpl();
				us.register(user);
				MailUtils.sendMail(user.getEmail(), user.getCode());
				request.setAttribute("msg", "<h5 style='color:green'>注册成功，请到邮箱激活</h5>");
			} catch (Exception e) {
				// TODO: handle exception
				throw new RuntimeException("封装数据异常" + e);
			}
			return "login.jsp";
		}else{
			request.setAttribute("vmsg", "<h6 style='color:red'>验证失败</h6>");
			return "register.jsp";
		}
	}
}