package cn.itcast.store.admin.web.servlet;

import java.util.List;

import cn.itcast.store.order.domain.Orders;
import cn.itcast.store.utils.PageBean;

public interface IAdminService {

	List<Orders> findAllOrderForPage(PageBean pb);

	Orders findOrderByOid(String oid);

}
