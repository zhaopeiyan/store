package cn.itcast.store.admin.web.dao;

import java.util.List;

import cn.itcast.store.admin.web.dao.impl.AdminDaoImpl;
import cn.itcast.store.admin.web.servlet.IAdminService;
import cn.itcast.store.order.domain.Orders;
import cn.itcast.store.utils.PageBean;

public class AdminServiceImpl implements IAdminService {

	@Override
	public List<Orders> findAllOrderForPage(PageBean pb) {
		// TODO Auto-generated method stub
		IAdminDao ad = new AdminDaoImpl();
		//设定每页显示几条记录
		pb.setPageSize(6);
		//查询订单总记录条数
		int record = ad.findAllOrder();
		pb.setTotalRecord(record);
		return ad.findAllOrderForPage(pb);
	}

	@Override
	public Orders findOrderByOid(String oid) {
		// TODO Auto-generated method stub
		IAdminDao ad = new AdminDaoImpl();
		
		return ad.findOrderByOid(oid);
	}

}
