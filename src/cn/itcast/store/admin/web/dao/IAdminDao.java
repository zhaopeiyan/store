package cn.itcast.store.admin.web.dao;

import java.util.List;

import cn.itcast.store.order.domain.Orders;
import cn.itcast.store.utils.PageBean;

public interface IAdminDao {

	int findAllOrder();

	List<Orders> findAllOrderForPage(PageBean pb);

	Orders findOrderByOid(String oid);

}
