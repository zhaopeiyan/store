package cn.itcast.store.admin.web.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import cn.itcast.store.admin.web.dao.IAdminDao;
import cn.itcast.store.order.domain.OrderItem;
import cn.itcast.store.order.domain.Orders;
import cn.itcast.store.product.domain.Product;
import cn.itcast.store.utils.C3P0Utils;
import cn.itcast.store.utils.PageBean;

public class AdminDaoImpl implements IAdminDao {

	/**
	 * 查询所有order表中记录
	 */
	@Override
	public int findAllOrder() {
		// TODO Auto-generated method stub
		Long i = 0l;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select count(*) from orders";
		try {
			i = (Long) qr.query(sql, new ScalarHandler());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询所有order表中记录异常" + e);
		}
		return i.intValue();
	}

	/**
	 * 分页查询所有order信息
	 */
	@Override
	public List<Orders> findAllOrderForPage(PageBean pb) {
		// TODO Auto-generated method stub
		List<Orders> orders = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		try {
			String sql = "select * from orders o,orderitem oi where oi.oid=o.oid limit ?,?";
			orders = qr.query(sql, new BeanListHandler<>(Orders.class), pb.getStartIndex(), pb.getPageSize());
			if (!orders.isEmpty()) {
				for (Orders order : orders) {
					OrderItem oi = new OrderItem();
					sql = "select * from orderitem oi,product p where p.pid=oi.pid and oi.oid=?";
					// 得到级联查询所有字段
					List<Map<String, Object>> map = qr.query(sql, new MapListHandler(), order.getOid());
					// 遍历查询到的数据
					// 定义orderitemList,封装数据
					List<OrderItem> list = new ArrayList<>();
					for (Map<String, Object> m : map) {
						BeanUtils.populate(list, m);
						Product product = new Product();
						BeanUtils.populate(product, m);
						oi.setProduct(product);
						list.add(oi);
					}
					order.setList(list);
				}
			} else {
				return orders;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		return orders;
	}

	/**
	 * 查询单个订单的详细信息
	 */
	@Override
	public Orders findOrderByOid(String oid) {
		// TODO Auto-generated method stub
		Orders order = null;
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "select * from orders where oid=?";
			order = qr.query(sql, new BeanHandler<>(Orders.class), oid);
			if(order != null){
				//级联查询订单表+订单项表+商品表
				sql = "select * from orderitem o,product p where p.pid = o.pid and o.oid=?";
				List<Map<String, Object>> map = qr.query(sql, new MapListHandler(), oid);
				List<OrderItem> olist = new ArrayList<>();
				//遍历map集合,封装order表中所要的数据
				for (Map<String, Object> m : map) {
					OrderItem oi = new OrderItem();
					BeanUtils.populate(oi, m);
					Product product = new Product();
					BeanUtils.populate(product, m);
					oi.setProduct(product);
					olist.add(oi);
				}
				order.setList(olist);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询单个订单的详细信息" + e);
		}
		return order;
	}
}
