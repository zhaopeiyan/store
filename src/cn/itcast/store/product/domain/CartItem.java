package cn.itcast.store.product.domain;

import java.io.Serializable;

public class CartItem implements Serializable{
	//购物项中的商品数量
	private int count;
	private Product product;
	//购物项的商品总金额
	private double subtotal;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public double getSubtotal() {
		return subtotal = this.getCount() * this.getProduct().getShop_price();
	}
	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}
	
}
