package cn.itcast.store.product.domain;

import java.io.Serializable;
import java.util.LinkedHashMap;

public class Cart implements Serializable{
	//总金额必须有初始值
	private double totalPrice = 0.0;
	//使用linkedHashMap存储购物项信息，便于删除操作，且是有序的
	private LinkedHashMap<String, CartItem> map = new LinkedHashMap<>();
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public LinkedHashMap<String, CartItem> getMap() {
		return map;
	}
	public void setMap(LinkedHashMap<String, CartItem> map) {
		this.map = map;
	}
	public Cart() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * 添加购物车里的购物项，如果有则累加，没有则添加
	 * @param ci
	 * @param product 
	 */
	public void addCartItem(CartItem ci, Product product) {
		// TODO Auto-generated method stub
		String pid = ci.getProduct().getPid();
		CartItem old = map.get(pid);
		if(old == null){
			map.put(pid, ci);
		}else{
//			old.setProduct(product);
			old.setCount(old.getCount() + ci.getCount());
		}
		totalPrice += ci.getSubtotal();
	}
	
	/**\
	 * 删除购物项
	 * @param cart
	 * @param pid
	 */
	public void delCartItem(Cart cart, String pid) {
		// TODO Auto-generated method stub
		totalPrice -= cart.map.get(pid).getSubtotal();
		cart.map.remove(pid);
	}
	
	/**
	 * 清空购物车
	 */
	public void clearCart() {
		// TODO Auto-generated method stub
		map.clear();
		totalPrice = 0.0;
	}
}
