package cn.itcast.store.product.web.dao;

import java.util.List;

import cn.itcast.store.product.domain.Category;
import cn.itcast.store.product.domain.Product;
import cn.itcast.store.utils.PageBean;

public interface IProductDao {

	List<Category> findAllCategory();

	List<Product> findProductByCategory(PageBean pb, String cid);

	int getAllProductByCategory(String cid);

	Product findProductByPid(String pid);

	List<Product> findProductHot();

	List<Product> findProductNew();

	void addCategory(Category category);

	void delCategory(String cid);

	Category findCategoryByCid(String cid);

	void modifyCategory(Category category);

	void addProduct(Product product);

	List<Product> findAllProduct(PageBean pb);

	int findAllProductRecord();

	void deleteProduct(String pid);

}
