package cn.itcast.store.product.web.dao.impl;

import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.sun.org.apache.regexp.internal.recompile;

import cn.itcast.store.product.domain.Category;
import cn.itcast.store.product.domain.Product;
import cn.itcast.store.product.web.dao.IProductDao;
import cn.itcast.store.utils.C3P0Utils;
import cn.itcast.store.utils.PageBean;

public class ProductDaoImpl implements IProductDao {

	/**
	 * 查询所有分类信息
	 */
	@Override
	public List<Category> findAllCategory() {
		// TODO Auto-generated method stub
		List<Category> categorys = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from category";
		try {
			categorys = qr.query(sql, new BeanListHandler<>(Category.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询所有分类信息异常" + e);
		}
		return categorys;
	}

	/**
	 * 通过类别查询所有商品信息并分页
	 */
	@Override
	public List<Product> findProductByCategory(PageBean pb, String cid) {
		// TODO Auto-generated method stub
		List<Product> products = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		try {
			String sql = "select * from product where cid=? limit ?,?";
			Object[] params = {cid,pb.getStartIndex(),pb.getPageSize()};
			products = qr.query(sql, new BeanListHandler<>(Product.class), params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("分页查询商品信息异常" + e);
		}
		return products;
	}

	/**
	 * 获取每种商品的总记录条数
	 */
	@Override
	public int getAllProductByCategory(String cid) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		Long count = 0L;
		String sql = "select count(*) from product where cid=?";
		try {
			count = (Long)qr.query(sql, new ScalarHandler(), cid);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询所有同类商品总记录数异常" + e);
		}
		return count.intValue();
	}

	/**
	 * 通过pid查询商品详细信息
	 */
	@Override
	public Product findProductByPid(String pid) {
		// TODO Auto-generated method stub
		Product product = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from product where pid =?";
		try {
			product = qr.query(sql, new BeanHandler<>(Product.class),pid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("通过pid查询商品详细信息异常" + e);
		}
		return product;
	}

	/**
	 * 查询所有热门商品信息
	 */
	@Override
	public List<Product> findProductHot() {
		// TODO Auto-generated method stub
		List<Product> products = null;
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "select * from product where is_hot=? and pflag=0 limit 9";
			products = qr.query(sql, new BeanListHandler<>(Product.class), 1);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询热门商品信息异常" + e);
		}
		return products;
	}

	/**
	 * 查询最新商品信息
	 */
	@Override
	public List<Product> findProductNew() {
		List<Product> products = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from product where pflag=0 order by pdate desc limit 9";
		try {
			products = qr.query(sql, new BeanListHandler<>(Product.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询最新商品信息异常" + e);
		}
		return products;
	}

	/**
	 * 添加分类数据
	 */
	@Override
	public void addCategory(Category category) {
		// TODO Auto-generated method stub
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "insert into category values(?,?)";
			qr.update(sql, category.getCid(),category.getCname());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("添加分类信息异常" + e);
		}
	}

	/**
	 * 删除分类信息
	 */
	@Override
	public void delCategory(String cid) {
		// TODO Auto-generated method stub
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "delete from category where cid=?";
			qr.update(sql, cid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("删除分类信息异常" + e);
		}
	}

	/**
	 * 通过cid查询category信息
	 */
	@Override
	public Category findCategoryByCid(String cid) {
		// TODO Auto-generated method stub
		Category category = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "select * from category where cid=?";
		try {
			category = qr.query(sql, new BeanHandler<>(Category.class), cid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		return category;
	}

	/**
	 * 修改分类信息
	 */
	@Override
	public void modifyCategory(Category category) {
		// TODO Auto-generated method stub
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "update category set cname=? where cid=?";
			qr.update(sql, category.getCname(),category.getCid());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("修改分类信息异常" + e);
		}
	}

	/**
	 * 添加商品信息
	 */
	@Override
	public void addProduct(Product product) {
		// TODO Auto-generated method stub
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "insert into product values(?,?,?,?,?,?,?,?,?,?)";
			Object[] params = {product.getPid(),product.getPname(),product.getMarket_price(),
					product.getShop_price(),product.getPimage(),product.getPdate(),product.getIs_hot(),
					product.getPdesc(),product.getPflag(),product.getCategory().getCid()};
			qr.update(sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("添加商品信息异常" + e);
		}
	}

	/**
	 * 分页查询所有商品信息
	 */
	@Override
	public List<Product> findAllProduct(PageBean pb) {
		// TODO Auto-generated method stub
		List<Product> list = null; 
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "select * from product p,category c where p.cid=c.cid limit ?,?";
			list = qr.query(sql, new BeanListHandler<>(Product.class),pb.getStartIndex(),pb.getPageSize());
			for (Product product : list) {
				Category category = new Category();
				sql = "select * from category where cid=?";
				category = qr.query(sql, new BeanHandler<>(Category.class), product.getCid());
				product.setCategory(category);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("分页查询所有商品信息异常" + e);
		}
		return list;
	}

	/**
	 * 查询所有商品信息总记录数
	 */
	@Override
	public int findAllProductRecord() {
		// TODO Auto-generated method stub
		Long record = 0L;
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "select count(*) from product";
			record = (Long)qr.query(sql, new ScalarHandler());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询所有商品信息总记录数异常" + e);
		}
		return record.intValue();
	}

	/**
	 * 删除商品信息
	 */
	@Override
	public void deleteProduct(String pid) {
		// TODO Auto-generated method stub
		try {
			QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
			String sql = "delete from product where pid=?";
			qr.update(sql, pid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("删除商品信息异常" + e);
		}
	}
}
