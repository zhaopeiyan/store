package cn.itcast.store.product.web.servlet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

import cn.itcast.store.product.domain.Cart;
import cn.itcast.store.product.domain.CartItem;
import cn.itcast.store.product.domain.Category;
import cn.itcast.store.product.domain.Product;
import cn.itcast.store.product.utils.JedisUtils;
import cn.itcast.store.product.web.servlet.impl.ProductServiceImpl;
import cn.itcast.store.user.utils.CookieUtil;
import cn.itcast.store.utils.BaseServlet;
import cn.itcast.store.utils.PageBean;
import cn.itcast.store.utils.UUIDUtils;
import redis.clients.jedis.Jedis;

public class ProductServlet extends BaseServlet {

	private static final long serialVersionUID = 1L;

	/*=======================================admin操作商品信息========================================================*/
	
	public void deleteProduct(HttpServletRequest request,HttpServletResponse response) throws Exception{
		//1、获取pid
		String pid = request.getParameter("pid");
		//2、调用service层方法
		IProductService ps = new ProductServiceImpl();
		ps.deleteProduct(pid);
		//3、删除成功之后，执行分页查询
		PageBean pb = new PageBean<>();
		int pageNumber = 1;
		pb.setPageNumber(pageNumber);
		//2、调用service层方法
		pb = ps.findAllProductForPage(pb);
		request.setAttribute("pb", pb);
		request.getRequestDispatcher("admin/product/plist.jsp").forward(request, response);
	}
	
	/**
	 * 查询所有商品信息并分页显示
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	public void findAllProductForPage(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{
		//1、拿到pageNumber封装到pageBean
		PageBean pb = new PageBean<>();
		int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
		pb.setPageNumber(pageNumber);
		//2、调用service层方法
		IProductService ps = new ProductServiceImpl();
		pb = ps.findAllProductForPage(pb);
		request.setAttribute("pb", pb);
		request.getRequestDispatcher("admin/product/plist.jsp").forward(request, response);
	}
	
	/**
	 * 使用同步方式，查询mysql全部分类信息
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException 
	 */
	public void findAllCategoryForProduct(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{
		List<Category> categorys= null;
		//查询所有记录不需要参数，直接调用service层方法
		String come = request.getParameter("come");
		System.out.println(come);
		IProductService ps = new ProductServiceImpl();
		categorys = ps.findAllCategory();
		request.setAttribute("categorys", categorys);
		if(come.equals("add")){
			request.getRequestDispatcher("admin/product/save.jsp").forward(request, response);
		}else if(come.equals("modify")){
			request.getRequestDispatcher("admin/product/modifyProduct.jsp").forward(request, response);
		}
	} 
	
	/**
	 * 添加商品信息
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void addProduct(HttpServletRequest request,HttpServletResponse response) throws Exception{
		//1、获取所有数据放到一个map中
		Map<String, Object> map = new HashMap<>();
		//2、创建磁盘文件工厂
		DiskFileItemFactory factory = new DiskFileItemFactory();
		//设置缓存大小，如果文件的大小超过了缓冲区的大小，就会产生临时文件
		//factory.setSizeThreshold(10*1024*1024);
		//3、创建上传组件核心类(ServletFileUpload)用来解析request的字节流
		ServletFileUpload upload = new ServletFileUpload(factory);
		//解决中文文件名上传乱码问题
		upload.setHeaderEncoding("utf-8");
		//4、用核心类解析request的字节流(把字节流分隔封装成了多个对象，一个对象封装着一个表单输入项的信息)
		List<FileItem> list = upload.parseRequest(request);
		String filename = null;
		//5、遍历
		for (FileItem fileItem : list) {
			if(fileItem.isFormField()){
				//普通
				map.put(fileItem.getFieldName(), fileItem.getString("utf-8"));
			}else{
				//上传项
				//6、获得文件要上传的路径
				String realPath = request.getServletContext().getRealPath("/products/1");
				//7、获得要上传文件的名字
				filename = fileItem.getName();
				//8、获得文件的输入流
				InputStream inputStream = fileItem.getInputStream();
				//9、获得文件的输出流
				OutputStream outputStream = new FileOutputStream(realPath+"/"+filename);
				//10、对流拷贝
				IOUtils.copy(inputStream, outputStream);
				//11、关闭流资源
				outputStream.close();
				inputStream.close();
			}
		}
		//12、封装其他数据
		Product product = new Product();
		BeanUtils.populate(product, map);
		product.setPid(UUIDUtils.getUUID());
		product.setPimage("products/1/"+filename);
		product.setPdate(new Date());
		product.setPflag(0);
		Category category = new Category();
//		String cid = request.getParameter("cid");
		category.setCid((String)map.get("cid"));
//		System.out.println(cid);
//		category.setCid(cid);
		product.setCategory(category);
		
		//13、调用service层处理业务逻辑
		IProductService ps = new ProductServiceImpl();
		ps.addProduct(product);
		
		//14、重定向到首页
		response.sendRedirect(request.getContextPath()+"/index.jsp");
		
	}
	
	/*===================================================admin操作分类信息============================================*/
	/**
	 * 修改分类信息
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void modifyCategory(HttpServletRequest request,HttpServletResponse response) throws Exception{
		Category category = new Category();
		//1、封装数据
		BeanUtils.populate(category, request.getParameterMap());
		//2、调用service层处理业务逻辑
		IProductService ps = new ProductServiceImpl();
		ps.modifyCategory(category);
		//3、修改成功需要清除redis数据库里原来的category数据
		Jedis jedis = JedisUtils.getJedis();
		jedis.del("category");
		findAllCategory(request, response);
	}
	
	/**
	 * 根据cid获取category记录，用于修改时的数据回显
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void findCategoryByCid(HttpServletRequest request,HttpServletResponse response) throws Exception{
		Category category = null;
		//1、获取cid
		String cid = request.getParameter("cid");
		//2、调用service层查询到相应数据
		IProductService ps = new ProductServiceImpl();
		category = ps.findCategoryByCid(cid);
		request.setAttribute("category", category);
		request.getRequestDispatcher("/admin/category/modifyCategory.jsp").forward(request, response);
	}
	
	/**
	 * 删除分类信息
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void delCategoryByCid(HttpServletRequest request,HttpServletResponse response) throws Exception{
		//1、获取cid
		String cid = request.getParameter("cid");
		//2、调用service层
		IProductService ps = new ProductServiceImpl();
		ps.delCategoryByCid(cid);
		//3、删除成功需要清除redis数据库里原来的category数据
		Jedis jedis = JedisUtils.getJedis();
		jedis.del("category");
		findAllCategory(request,response);
	}
	
	/**
	 * 添加分类信息
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void addCategory(HttpServletRequest request,HttpServletResponse response) throws Exception{
		Category category = new Category();
		//1、获取参数cname
		String cname = request.getParameter("cname");
		//2、封装数据
		category.setCid(UUIDUtils.getUUID());
		category.setCname(cname);
		//3、调用service层方法
		IProductService ps = new ProductServiceImpl();
		ps.addCategory(category);
		
		//4、添加成功需要清除redis数据库里原来的category数据
		Jedis jedis = JedisUtils.getJedis();
		jedis.del("category");
		
		//5、重新调用查询所有category方法
		findAllCategory(request,response);
	}
	
	/*
	 * ===========================================购物车相关操作========================================================
	 */
	/**
	 * 删除购物项
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void delCart(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		//从页面获取到要删除的商品的pid
		String pid = request.getParameter("pid");
		Cart cart = (Cart) request.getSession().getAttribute("cart");
		cart.delCartItem(cart,pid);
		request.getSession().setAttribute("cart", cart);
		request.getRequestDispatcher("cart.jsp").forward(request, response);
	}
	
	
	/**
	 * 清空购物车
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	public void clearCart(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		//request.getSession().setAttribute("total", 0.0);
		Cart cart = (Cart) request.getSession().getAttribute("cart");
		cart.clearCart();
//		request.getSession().removeAttribute("cart");
		request.getRequestDispatcher("cart.jsp").forward(request, response);
	}
	
	/**
	 * 添加购物车
	 * @param request
	 * @param response
	 */
	public void addCart(HttpServletRequest request,HttpServletResponse response){
		try {
			//接收请求参数
			int count = Integer.parseInt(request.getParameter("count"));
			String pid = request.getParameter("pid");
			//通过pid查询商品信息
			IProductService ps = new ProductServiceImpl();
			Product product = ps.findProductByPid(pid);
			//将购物项信息完善
			CartItem ci = new CartItem();
			ci.setCount(count);
			ci.setProduct(product);
			//将购物项添加到购物车里
			Cart cart = this.getCart(request);
			cart.addCartItem(ci,product);
			request.getSession().setAttribute("cart", cart);
			//重定向
			response.sendRedirect(request.getContextPath()+"/cart.jsp");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
	}
	
	//拿到购物车对象
	public Cart getCart(HttpServletRequest request){
		Cart cart = null;
		//判断是否已经有了购物车session，如果有则追加到购物车，如果没有则创建购物车再添加购物项
		cart = (Cart) request.getSession().getAttribute("cart");
		if(cart == null){
			//直接追加
			cart = new Cart();
		}
		return cart;
	}
	
	/*
	 * ===========================================商品信息相关操作========================================================
	 */
	/**
	 * 异步查询最新商品信息
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void findProductNew(HttpServletRequest request,HttpServletResponse response) throws Exception{
		List<Product> newProduct = null;
		//没有请求数据，直接调用service层处理业务逻辑
		IProductService ps = new ProductServiceImpl();
		Gson gson = new Gson();
		newProduct = ps.findProductNew();
		if(newProduct != null){
			String json = gson.toJson(newProduct);
			response.getWriter().write(json);
		}else{
			response.getWriter().write("false");
		}
	}
	
	/**
	 * 异步查询热门商品信息
	 * @throws Exception 
	 */
	public void findProductHot(HttpServletRequest request,HttpServletResponse response) throws Exception{
		List<Product> hot = null;
		//没有请求数据，直接调用service层处理业务逻辑
		IProductService ps = new ProductServiceImpl();
		Gson gson = new Gson();
		hot = ps.findProductHot();
		if(hot != null){
			String json = gson.toJson(hot);
			response.getWriter().write(json);
		}else{
			response.getWriter().write("false");
		}
	}
	
	/**
	 * 将热门商品和最新商品的查询放在一个方法里，因为是同步操作，所以放到两个方法里会出现只执行一个方法的问题
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void findProductNewAndHot(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		List<Product> hot = null;
		List<Product> newproducts = null;
		//没有请求数据，直接调用service层处理业务逻辑
		IProductService ps = new ProductServiceImpl();
		hot = ps.findProductHot();
		newproducts = ps.findProductNew();
		if(hot != null || newproducts != null){
			request.setAttribute("newproducts", newproducts);
			request.setAttribute("hot", hot);
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}else{
			request.setAttribute("msg", "<h6 style='color:red'>对不起，没有查询到相关商品</h6>");
		}
	}
	
	/**
	 * 查询最新上架商品信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public void findProductNew(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		List<Product> products = null;
		//没有请求数据，直接调用service层处理业务逻辑
		IProductService ps = new ProductServiceImpl();
		products = ps.findProductNew();
		if(products != null){
			request.setAttribute("newproducts", products);
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}else{
			request.setAttribute("msg", "<h6 style='color:red'>对不起，没有查询到最新商品</h6>");
		}
	}*/
	
	/**
	 * 查询所有热门商品信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public void findProductHot(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		List<Product> products = null;
		//没有请求数据，直接调用service层处理业务逻辑
		IProductService ps = new ProductServiceImpl();
		products = ps.findProductHot();
		if(products != null){
			request.setAttribute("hot", products);
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}else{
			request.setAttribute("msg", "<h6 style='color:red'>对不起，没有查询到热门商品</h6>");
		}
	}*/
	
	/**
	 * 通过pid查询商品详细信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void findProductByPid(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		Product product = null;
		//1、获取点击的商品pid
		String pid = request.getParameter("pid");
		System.out.println(pid);
		//添加历史浏览cookie记录
		Cookie[] cookies = request.getCookies();
		Cookie cookie = CookieUtil.findCookie(cookies, "history");
		if(cookie == null){
			cookie = new Cookie("history",pid);
			cookie.setPath("/");
			cookie.setMaxAge(60*60);
			response.addCookie(cookie);
		}else{
			String value = cookie.getValue();
			String[] pids = value.split("-");
			LinkedList<String> list = new LinkedList<String>(Arrays.asList(pids));
			if(list.contains(pid)){
				//已经浏览过
				list.remove(pid);
				list.addFirst(pid);
			}else{
				//没有浏览过的商品
				if(list.size() >= 6){
					list.removeLast();
					list.addFirst(pid);
				}else{
					list.addFirst(pid);
				}
			}
			request.setAttribute("history", list);
		}
		//2、调用service层
		IProductService ps = new ProductServiceImpl();
		product = ps.findProductByPid(pid);
		if(product != null){
			request.setAttribute("product", product);
		}else{
			request.setAttribute("product", "<h5>对不起没有查询到有效数据</h5>");
		}
		request.getRequestDispatcher("product_info.jsp").forward(request, response);
	}
	
	/**
	 * 分页查询所有同类商品信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void findProductByCategory(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		PageBean pb = new PageBean<>();
		//1、获取pageNumber和cid
		int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
		//System.out.println(pageNumber);
		String cid = request.getParameter("cid");
		//System.out.println(cid);
		//2、封装pagebean
		pb.setPageNumber(pageNumber);
		//3、调用service层，处理业务逻辑
		IProductService ps = new ProductServiceImpl();
		List<Product> products = ps.findProductByCategory(pb,cid);
		pb.setResult(products);
		int totalPage = pb.getTotalPage();
		//System.out.println("总页数"+totalPage);
		if(products != null){
			request.setAttribute("pageBean", pb);
		}else{
			request.setAttribute("msg", "<h5 style='color:red'>对不起没有查询到相关数据</h5>");
		}
		request.getRequestDispatcher("product_list.jsp").forward(request, response);
	}
	
	/**
	 * 查询所有分类信息redis
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public void findAllCategoryByRedis(HttpServletRequest request,HttpServletResponse response) throws IOException{
		Jedis jedis = new JedisUtils().getJedis();
		String json = jedis.get("category");
		//System.out.println(json);
		Gson gson = new Gson();
		if(json == null){
			//缓存没数据
			//System.out.println("redis中没有数据，查询mysql数据库");
			IProductService ps = new ProductServiceImpl();
			List<Category> categorys = ps.findAllCategory();
			if(categorys != null){
				json = gson.toJson(categorys);
				jedis.set("category", json);
			}
		}else{
			//缓存有数据
			//System.out.println("redis中有数据，直接返回json数据");
		}
		response.getWriter().write(json);
	}
	
	/**
	 * 使用同步方式，查询mysql全部分类信息
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException 
	 */
	public void findAllCategory(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{
		List<Category> categorys= null;
		//查询所有记录不需要参数，直接调用service层方法
		IProductService ps = new ProductServiceImpl();
		categorys = ps.findAllCategory();
		request.setAttribute("categorys", categorys);
		request.getRequestDispatcher("admin/category/clist.jsp").forward(request, response);
	} 
}