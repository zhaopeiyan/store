package cn.itcast.store.product.web.servlet;

import java.util.List;

import cn.itcast.store.product.domain.Category;
import cn.itcast.store.product.domain.Product;
import cn.itcast.store.utils.PageBean;

public interface IProductService {

	List<Category> findAllCategory();

	List<Product> findProductByCategory(PageBean pb, String cid);

	Product findProductByPid(String pid);

	List<Product> findProductHot();

	List<Product> findProductNew();

	void addCategory(Category category);

	void delCategoryByCid(String cid);

	Category findCategoryByCid(String cid);

	void modifyCategory(Category category);

	void addProduct(Product product);

	PageBean findAllProductForPage(PageBean pb);

	void deleteProduct(String pid);

}
