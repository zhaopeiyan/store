package cn.itcast.store.product.web.servlet.impl;

import java.util.List;

import cn.itcast.store.product.domain.Category;
import cn.itcast.store.product.domain.Product;
import cn.itcast.store.product.web.dao.IProductDao;
import cn.itcast.store.product.web.dao.impl.ProductDaoImpl;
import cn.itcast.store.product.web.servlet.IProductService;
import cn.itcast.store.utils.PageBean;

public class ProductServiceImpl implements IProductService {

	/**
	 * 查询所有分类信息
	 */
	@Override
	public List<Category> findAllCategory() {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		return pd.findAllCategory();
	}

	/**
	 * 分页查询同类商品信息，配置pageBean类中的参数
	 */
	@Override
	public List<Product> findProductByCategory(PageBean pb, String cid) {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		//设定每页显示的记录数
		pb.setPageSize(12);
		//获得总记录条数
		int count = pd.getAllProductByCategory(cid);
		pb.setTotalRecord(count);
		//获取总页数
		int totalPage = (count % pb.getPageSize() == 0) ? (pb.getTotalRecord() / pb.getPageSize())
                : (pb.getTotalRecord() / pb.getPageSize() + 1);
		pb.setTotalPage(totalPage);
		//获取起始id
		int startIndex = (pb.getPageNumber() - 1) * pb.getPageSize();
		pb.setStartIndex(startIndex);
		return pd.findProductByCategory(pb,cid);
	}

	/**
	 * 通过pid查询商品详细信息
	 */
	@Override
	public Product findProductByPid(String pid) {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		return pd.findProductByPid(pid);
	}

	/**
	 * 查询所有热门商品
	 */
	@Override
	public List<Product> findProductHot() {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		return pd.findProductHot();
	}

	/**
	 * 查询最新商品信息
	 */
	@Override
	public List<Product> findProductNew() {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		return pd.findProductNew();
	}

	/**
	 * 添加分类信息
	 */
	@Override
	public void addCategory(Category category) {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		pd.addCategory(category);
	}

	/**
	 * 删除分类信息
	 */
	@Override
	public void delCategoryByCid(String cid) {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		pd.delCategory(cid);
	}

	/**
	 * 通过cid查询相应的category表数据
	 */
	@Override
	public Category findCategoryByCid(String cid) {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		return pd.findCategoryByCid(cid);
	}

	/**
	 * 修改分类信息
	 */
	@Override
	public void modifyCategory(Category category) {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		pd.modifyCategory(category);
	}

	/**
	 * 添加商品信息
	 */
	@Override
	public void addProduct(Product product) {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		pd.addProduct(product);
	}

	@Override
	public PageBean findAllProductForPage(PageBean pb) {
		// TODO Auto-generated method stub
		pb.setPageSize(10);
		IProductDao pd = new ProductDaoImpl();
		int record = pd.findAllProductRecord();
		pb.setTotalRecord(record);
		List<Product> list = pd.findAllProduct(pb);
		pb.setResult(list);
		return pb;
	}

	@Override
	public void deleteProduct(String pid) {
		// TODO Auto-generated method stub
		IProductDao pd = new ProductDaoImpl();
		pd.deleteProduct(pid);
	}
}
