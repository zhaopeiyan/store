package cn.itcast.store.utils;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class BaseServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String methodName = req.getParameter("method");
		if(methodName != null){
			Class clazz = this.getClass();
			try {
				@SuppressWarnings("unchecked")
				Method method = clazz.getMethod(methodName, HttpServletRequest.class,HttpServletResponse.class);
				if(method != null){
					String path = (String)method.invoke(this,req, resp);
					System.out.println(methodName);
					if(path != null){
						req.getRequestDispatcher(path).forward(req, resp);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				throw new RuntimeException("执行"+methodName+"方法异常" + e);
			}
		}else{
			System.out.println("获取不到指定的方法名");
		}
	}
}
