package cn.itcast.store.order.utils;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.store.user.domain.User;

/**
 * Servlet Filter implementation class PrivligeFilter
 */
public class PrivligeFilter implements Filter {

    /**
     * Default constructor. 
     */
    public PrivligeFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		/**
		 * 如果用户未登陆，让其登陆，否则放行，访问目标资源
		 */
		//1、强转
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		//2、从session中获取existUser
		User existUser = (User) req.getSession().getAttribute("existUser");
		//3、判断existUser是否为空
		if(existUser == null){
			req.setAttribute("msg", "<h6 style='color:red'>查询订单请先登陆</h6>");
//			req.getRequestDispatcher("login.jsp").forward(req, res);
			res.setHeader("Refresh", "0,url=http://localhost:8080/store/login.jsp");
		}else{
			chain.doFilter(request, response);
		}
		// pass the request along the filter chain
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
