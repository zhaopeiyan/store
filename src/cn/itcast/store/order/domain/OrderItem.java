package cn.itcast.store.order.domain;

import java.io.Serializable;

import cn.itcast.store.product.domain.Product;

public class OrderItem implements Serializable{
	private String itemid;
	private int count;
	private double subtotal;
	private Orders order;
	private Product product;
	
	
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public double getSubtotal() {
		return subtotal = this.getProduct().getShop_price() * this.getCount();
	}
	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}
	public Orders getOrder() {
		return order;
	}
	public void setOrder(Orders order) {
		this.order = order;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public OrderItem() {
		
	}
}
