package cn.itcast.store.order.web.dao;

import java.sql.Connection;
import java.util.List;

import cn.itcast.store.order.domain.OrderItem;
import cn.itcast.store.order.domain.Orders;
import cn.itcast.store.utils.PageBean;

public interface IOrderDao {

	void makeOrder(Connection conn, Orders orders);

	void addOrderItem(Connection conn, OrderItem orderItem);

	int finAllOrderRecordByUid(String uid);

	List<Orders> findAllOrders(String uid, PageBean pb);

	Orders orderDetail(String oid);

	void modify(Orders order);

}
