package cn.itcast.store.order.web.dao.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import cn.itcast.store.order.domain.OrderItem;
import cn.itcast.store.order.domain.Orders;
import cn.itcast.store.order.web.dao.IOrderDao;
import cn.itcast.store.product.domain.Product;
import cn.itcast.store.utils.C3P0Utils;
import cn.itcast.store.utils.PageBean;

public class OrderDaoImpl implements IOrderDao {

	/**
	 * 添加订单
	 */
	@Override
	public void makeOrder(Connection conn, Orders orders) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner();
		try {
			String sql = "insert into orders values(?,?,?,?,?,?,?,?)";
			Object[] params = {orders.getOid(),orders.getOrdertime(),orders.getTotal(),
				orders.getState(),orders.getAddress(),orders.getName(),
				orders.getTelephone(),orders.getUser().getUid()};
			qr.update(conn, sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("添加orders数据异常" + e);
		}
	}

	/**
	 * 添加订单项
	 */
	@Override
	public void addOrderItem(Connection conn, OrderItem orderItem) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner();
		try {
			String sql = "insert into orderitem values(?,?,?,?,?)";
			Object[] params = {orderItem.getItemid(),orderItem.getCount(),
				orderItem.getSubtotal(),orderItem.getProduct().getPid(),orderItem.getOrder().getOid()};
			qr.update(conn, sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("添加orderitem数据异常" + e);
		}
	}

	/**
	 * 根据用户查询所有订单记录条数
	 */
	@Override
	public int finAllOrderRecordByUid(String uid) {
		// TODO Auto-generated method stub
		Long i = 0l;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		try {
			String sql = "select count(*) from orders where uid=?";
			i = (Long) qr.query(sql, new ScalarHandler(), uid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询所有订单记录数异常" + e);
		}
		return i.intValue();
	}

	/**
	 * 根据uid查询所有订单详细信息
	 */
	@Override
	public List<Orders> findAllOrders(String uid,PageBean pb) {
		// TODO Auto-generated method stub
		List<Orders> orderList = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		try {
			//1、先查询所有orders表中记录返回一个待填充的List集合
			String sql = "select * from orders where uid=? order by ordertime desc limit ?,?";
			orderList = qr.query(sql, new BeanListHandler<>(Orders.class), uid,pb.getStartIndex(),pb.getPageSize());
			//2、判断list集合是否为空
			if(!orderList.isEmpty()){
				//3、不为空查询所有与orderList集合里oid相同的orderitem记录，需要遍历orderList
				for (Orders order : orderList) {
					//4、级联查询
					sql = "select * from product p,orderitem o where o.pid=p.pid and o.oid=?";
					List<Map<String,Object>> map = qr.query(sql, new MapListHandler(),order.getOid());
					//创建一个orderitem集合，用于封装单个orderitem，将其保存到orders对象中
					ArrayList<OrderItem> oiList = new ArrayList<>();
					//5、遍历map集合
					for (Map<String, Object> list : map) {
						//6、将订单项添加到
						OrderItem oi = new OrderItem();
						BeanUtils.populate(oi, list);
						Product product = new Product();
						BeanUtils.populate(product, list);
						oi.setProduct(product);
						oiList.add(oi);
					}
					order.setList(oiList);
				}
			}else{
				return orderList;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("根据uid查询所有订单详细信息异常" + e);
		}
		return orderList;
	}

	
	/**
	 * 查询订单详细信息
	 */
	@Override
	public Orders orderDetail(String oid) {
		// TODO Auto-generated method stub
		Orders order = null;
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		try {
			String sql = "select * from orders where oid=?";
			order = qr.query(sql, new BeanHandler<>(Orders.class), oid);
			if(order != null){
				sql = "select * from product p,orderitem o where p.pid=o.pid and o.oid=?";
				List<OrderItem> oilist = new ArrayList<>();
				List<Map<String,Object>> list = qr.query(sql, new MapListHandler(), oid);
				for (Map<String, Object> map : list) {
					OrderItem oi = new OrderItem();
					BeanUtils.populate(oi, map);
					Product product = new Product();
					BeanUtils.populate(product, map);
					oi.setProduct(product);
					oilist.add(oi);
				}
				order.setList(oilist);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("查询订单详细信息异常" + e);
		}
		return order;
	}

	/**
	 * 支付成功修改订单状态
	 */
	@Override
	public void modify(Orders order) {
		// TODO Auto-generated method stub
		QueryRunner qr = new QueryRunner(C3P0Utils.getDataSource());
		String sql = "update order set state=? where oid=?";
		try {
			qr.update(sql,order.getState(),order.getOid());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("支付成功修改订单状态异常" + e);
		}
	}
}
