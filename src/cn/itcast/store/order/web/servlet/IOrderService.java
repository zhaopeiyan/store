package cn.itcast.store.order.web.servlet;

import cn.itcast.store.order.domain.Orders;
import cn.itcast.store.utils.PageBean;

public interface IOrderService {

	void makeOrder(Orders orders);

	PageBean findAllOrders(PageBean pb, String string);

	Orders orderDetail(String oid);

	void modify(Orders order);

}
