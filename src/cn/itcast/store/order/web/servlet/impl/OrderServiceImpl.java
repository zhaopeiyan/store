package cn.itcast.store.order.web.servlet.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import cn.itcast.store.order.domain.OrderItem;
import cn.itcast.store.order.domain.Orders;
import cn.itcast.store.order.web.dao.IOrderDao;
import cn.itcast.store.order.web.dao.impl.OrderDaoImpl;
import cn.itcast.store.order.web.servlet.IOrderService;
import cn.itcast.store.utils.C3P0Utils;
import cn.itcast.store.utils.PageBean;

public class OrderServiceImpl implements IOrderService {

	/**
	 * 生成订单项表字段和订单表字段
	 */
	@Override
	public void makeOrder(Orders orders) {
		// TODO Auto-generated method stub
		//通过c3p0utils工具包得到线程安全的数据库连接对象
		Connection conn = C3P0Utils.getConnnection();
		//开启事务
		try {
			conn.setAutoCommit(false);
			//调用数据访问层
			IOrderDao od = new OrderDaoImpl();
			od.makeOrder(conn,orders);
			for(OrderItem orderItem:orders.getList()){
				od.addOrderItem(conn,orderItem);
			}
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e.printStackTrace();
				throw new RuntimeException("数据回滚异常"+e);
			}
			e.printStackTrace();
			throw new RuntimeException("提交数据异常"+e);
		}
	}

	@Override
	public PageBean findAllOrders(PageBean pb,String uid) {
		// TODO Auto-generated method stub
		//1、设置每页显示的信息条数
		pb.setPageSize(3);
		IOrderDao od = new OrderDaoImpl();
		//2、根据uid,查询order表中的总计录数
		int record = od.finAllOrderRecordByUid(uid);
		pb.setTotalRecord(record);
		//3、根据uid查询所有订单详细记录，包括商品信息，包括订单项信息，包括订单信息
		List<Orders> orders = od.findAllOrders(uid,pb);
		pb.setResult(orders);
		return pb;
	}

	@Override
	public Orders orderDetail(String oid) {
		// TODO Auto-generated method stub
		IOrderDao od = new OrderDaoImpl();
		return od.orderDetail(oid);
	}

	/**
	 * 支付成功修改订单状态
	 */
	@Override
	public void modify(Orders order) {
		// TODO Auto-generated method stub
		IOrderDao od = new OrderDaoImpl();
		od.modify(order);
	}
}
