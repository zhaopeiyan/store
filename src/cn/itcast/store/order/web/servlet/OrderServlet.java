package cn.itcast.store.order.web.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import cn.itcast.store.order.domain.OrderItem;
import cn.itcast.store.order.domain.Orders;
import cn.itcast.store.order.web.servlet.impl.OrderServiceImpl;
import cn.itcast.store.product.domain.Cart;
import cn.itcast.store.product.domain.CartItem;
import cn.itcast.store.user.domain.User;
import cn.itcast.store.utils.BaseServlet;
import cn.itcast.store.utils.PageBean;
import cn.itcast.store.utils.PaymentUtil;
import cn.itcast.store.utils.UUIDUtils;

public class OrderServlet extends BaseServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * 支付成功执行修改订单状态方法
	 * @param request
	 * @param response
	 * @return 
	 */
	public String payOrderSuccess(HttpServletRequest request,HttpServletResponse response){
		//订单支付成功，修改订单状态为1，即已支付      使用老师给的回调接口
		//获取返回的所有参数
		String p1_MerId = request.getParameter("p1_MerId");
		String r0_Cmd = request.getParameter("r0_Cmd");
		String r1_Code = request.getParameter("r1_Code");
		String r2_TrxId = request.getParameter("r2_TrxId");
		String r3_Amt = request.getParameter("r3_Amt");
		String r4_Cur = request.getParameter("r4_Cur");
		String r5_Pid = request.getParameter("r5_Pid");
		String r6_Order = request.getParameter("r6_Order");
		String r7_Uid = request.getParameter("r7_Uid");
		String r8_MP = request.getParameter("r8_MP");
		String r9_BType = request.getParameter("r9_BType");
		String rb_BankId = request.getParameter("rb_BankId");
		String ro_BankOrderId = request.getParameter("ro_BankOrderId");
		String rp_PayDate = request.getParameter("rp_PayDate");
		String rq_CardNo = request.getParameter("rq_CardNo");
		String ru_Trxtime = request.getParameter("ru_Trxtime");
		String hmac = request.getParameter("hmac");

		//校验数据是否正确
		boolean flag = PaymentUtil.verifyCallback(hmac, p1_MerId, r0_Cmd, r1_Code, r2_TrxId, r3_Amt, r4_Cur, r5_Pid, r6_Order, r7_Uid, r8_MP, r9_BType, "69cl522AV6q613Ii4W6u8K6XuW8vM1N6bFgyv769220IuYe9u37N4y7rI4Pl");
		if(flag){
			//数据正确,修改订单状态
			IOrderService service = new OrderServiceImpl();
			try{
				Orders order = service.orderDetail(r6_Order);
				order.setState(1);
				service.modify(order);
				request.setAttribute("msg","订单付款成功,订单号为:"+r6_Order+"///付款金额为:"+r3_Amt);
			}catch(Exception e){
				throw new RuntimeException(e);
			}
		}else{
			throw new RuntimeException("数据遭篡改");
		}
		return "order_list.jsp";
	}
	
	/**
	 * 在线支付功能实现
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @throws Exception
	 */
	public void payforOrder (HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		Orders order = new Orders();
		//1、获取method和oid
		BeanUtils.populate(order, request.getParameterMap());
		//复制过来的付款接口
		String p0_Cmd = "Buy";
		String p1_MerId = "10001126856";
		String p2_Order = order.getOid();
		String p3_Amt = "0.01";//测试用1分钱，真正开发中用order.getTotal();
		String p4_Cur = "CNY";
		String p5_Pid = "";
		String p6_Pcat = "";
		String p7_Pdesc = "";
		String p8_Url = "http://localhost:8080"+request.getContextPath()+"/OrderServlet?method=payOrderSuccess";
		String p9_SAF = "0";
		String pa_MP = "";
		String pd_FrpId = request.getParameter("pd_FrpId");
		String pr_NeedResponse = "1";
		String hmac = PaymentUtil.buildHmac(p0_Cmd, p1_MerId, p2_Order, p3_Amt, p4_Cur, p5_Pid, p6_Pcat, p7_Pdesc, p8_Url, p9_SAF, pa_MP, pd_FrpId, pr_NeedResponse, "69cl522AV6q613Ii4W6u8K6XuW8vM1N6bFgyv769220IuYe9u37N4y7rI4Pl");

		StringBuffer buffer = new StringBuffer("https://www.yeepay.com/app-merchant-proxy/node?");
		buffer.append("p0_Cmd="+p0_Cmd);
		buffer.append("&p1_MerId="+p1_MerId);
		buffer.append("&p2_Order="+p2_Order);
		buffer.append("&p3_Amt="+p3_Amt);
		buffer.append("&p4_Cur="+p4_Cur);
		buffer.append("&p5_Pid="+p5_Pid);
		buffer.append("&p6_Pcat="+p6_Pcat);
		buffer.append("&p7_Pdesc="+p7_Pdesc);
		buffer.append("&p8_Url="+p8_Url);
		buffer.append("&p9_SAF="+p9_SAF);
		buffer.append("&pa_MP="+pa_MP);
		buffer.append("&pd_FrpId="+pd_FrpId);
		buffer.append("&pr_NeedResponse="+pr_NeedResponse);
		buffer.append("&hmac="+hmac);

		response.sendRedirect(buffer.toString());
	}
	
	/**
	 * 查询订单详情
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void orderDetail (HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//1、获取要查询订单的oid
		String oid = request.getParameter("oid");
		//2、调用service层
		IOrderService os = new OrderServiceImpl();
		Orders order = os.orderDetail(oid);
		request.setAttribute("orders", order);
		request.getRequestDispatcher("order_info.jsp").forward(request, response);
	}
	
	/**
	 * 查询所有订单信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void findAllOrders(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//1、判断用户登录是否超时
		User existUser = null;
		existUser = (User) request.getSession().getAttribute("existUser");
		if(existUser != null){
			//2、用户还在登陆状态中，执行操作
			PageBean pb = new PageBean<>();
			int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
			pb.setPageNumber(pageNumber);
			IOrderService os = new OrderServiceImpl();
			pb = os.findAllOrders(pb,existUser.getUid());
			
			if(pb != null && pb.getResult().size() > 0){
				request.setAttribute("pb", pb);
			}else{
				request.setAttribute("msg", "<h6 style='color:red'>您还没有订单</h6>");
			}
			request.getRequestDispatcher("order_list.jsp").forward(request, response);
		}else{
			request.setAttribute("msg", "<h6 style='color:red'>请先登录进行相关操作</h6>");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}
	
	/**
	 * 生成订单列表
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	public void makeOrderList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 判断用户是否登陆
		User existUser = (User) request.getSession().getAttribute("existUser");
		if (existUser == null) {
			request.setAttribute("msg", "<h6 style='color:red'>请先登录，进行结算</h6>");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		} else {
			Cart cart = (Cart) request.getSession().getAttribute("cart");
			// 判断购物车是否为空
			if(cart == null){
				request.setAttribute("msg", "<h6 style='color:red'>清先添加商品再进行结算</h6>");
				request.getRequestDispatcher("cart.jsp").forward(request, response);
			}else{
				if (cart.getMap().size() == 0) {
					request.setAttribute("msg", "<h6 style='color:red'>清先添加商品再进行结算</h6>");
					request.getRequestDispatcher("cart.jsp").forward(request, response);
				} else {
					// 使用cart填补orders信息
					Orders orders = new Orders();
					orders.setOid(UUIDUtils.getUUID());
					orders.setOrdertime(new Date());
					orders.setState(0);
					orders.setTotal(cart.getTotalPrice());
					orders.setUser(existUser);
					// 使用cart填充orderItem信息
					List<OrderItem> orderList = new ArrayList<OrderItem>();
					Set<Entry<String, CartItem>> entrySet = cart.getMap().entrySet();
					for (Entry<String, CartItem> entry : entrySet) {
						OrderItem oi = new OrderItem();
						oi.setItemid(UUIDUtils.getUUID());
						oi.setOrder(orders);
						oi.setCount(entry.getValue().getCount());
						oi.setSubtotal(entry.getValue().getSubtotal());
						oi.setProduct(entry.getValue().getProduct());
						orderList.add(oi);
					}
					// 将订单集合存入订单中
					orders.setList(orderList);
					// 调用service层
					IOrderService os = new OrderServiceImpl();
					os.makeOrder(orders);
					cart.clearCart(); 
					request.setAttribute("orders", orders);
					request.getRequestDispatcher("order_info.jsp").forward(request, response);
				}
			}
		}
	}
}